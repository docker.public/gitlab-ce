# GitLab Docker Image
###### Based on Official GitLab Docker image (https://hub.docker.com/r/gitlab/gitlab-ce)

# Requirements
+ Edit file .env

# Running
```
docker-compose up -d
```
NOTE: The very first time you visit GitLab, you will be asked to set up the admin password. After you change it, you can login with username root and the password you set up.

# More Info
+ https://docs.gitlab.com/omnibus/docker/